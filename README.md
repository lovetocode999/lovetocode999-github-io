# lovetocode999.github.io

This is the repository for my website, where I post my coding stuff and play with html😁


## Goals:

* viewable in text mode browsers (e.g. lynx or w3m)
* easy to create new pages using templates or an api
* looks nice (with bootstrap)
* has a reasonable file structure
* <details><summary>no google</summary> I believe that people should be able to browse the web freely without being spied on by large companies, so I will completely eliminate all use of Google analytics, or any of their other products</details>


## Why?
I wanted more practice doing web development and my own website was very outdated. So, I decided I would rewrite it with the aforementioned goals in mind.


## Website Plan:
* ### page layout:
  * <details><summary>the header and footer should be the same across pages, with only the content changing.</summary> I could possibly put header and footer files in an assets directory, and then load them with jquery. This way I would only need to change two files to change all the pages: https://stackoverflow.com/questions/18712338/make-header-and-footer-files-to-be-included-in-multiple-html-pages I could perhaps also use the theme to make the header always appear, while making the footer only appear when scrolled all the way down</details>
  * All the pages should have the same bootstrap theme

* ### file hierarchy:
  ```bash
  / #note this website plan is not definite, it could change in the future
  ├─ assets #assets folder
  │  ├─ js
  │  │  ├─ symlink to node_modules
  │  ├─ css
  │  │  ├─ bootstrap files #symlinked if necessary
  │  ├─ html
  │  │  ├─ header.html
  │  │  ├─ footer.html
  │  │  ├─ 404.html #a 404 file, this should be kinda obvious
  ├─ projects #folder for my non-experimental projects
  ├─ experiments #folder for my experimental projects
  ```
